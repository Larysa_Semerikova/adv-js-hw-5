const usersRequest = fetch("https://ajax.test-danit.com/api/json/users", {
  method: "GET",
});
usersRequest
  .then((response) => response.json())
  .then((users) => {
    sendRequest(users);
    return users;
  })
  .catch((err) => console.log(err));

function sendRequest(users) {
  const postsRequest = fetch("https://ajax.test-danit.com/api/json/posts", {
    method: "GET",
  });
  postsRequest
    .then((result) => result.json())
    .then((posts) => {
      const wrapper = document.querySelector("#cardsWrapper");
      posts.forEach((post) => {
        const user = users.find((user) => user.id === post.userId);
        const card = new Card(
          user.name,
          user.email,
          post.title,
          post.body,
          user.id,
          post.id
        );
        wrapper.append(card.render());
      });
    })
    .catch((err) => console.log(err));
}

class Card {
  constructor(name, email, title, body, userId, postId) {
    (this.name = name),
      (this.email = email),
      (this.title = title),
      (this.body = body),
      (this.userId = userId),
      (this.postId = postId);
  }

  render() {
    const cardBlock = document.createElement("div");
    cardBlock.id = `${this.postId}`;
    cardBlock.className = "cardBlock";
    cardBlock.innerHTML = `
        <div class="headerBlock">
        <div class="cardHeader">
        <h2 class="cardAuthor">${this.name}</h2>
        <span class="cardEmail">@${this.email}</span>
        </div>
        <div class="btn"><button onclick="cardDelete(${this.postId})" class="btn-close"></button></div>
        </div>
        <span class="cardTitle">${this.title}</span>
        <p class="cardBody">${this.body}</p>
       
        `;
    return cardBlock;
  }
}

function cardDelete(postId) {
  fetch(`https://ajax.test-danit.com/api/json/posts/${postId}`, {
    method: "DELETE",
    headers: {
      "Content-Type": "application/json",
    },
  }).then((response) => {
    if (response.ok) {
      document.getElementById(`${postId}`).remove();
    }
  });
}
